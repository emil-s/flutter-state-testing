import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:state_testing/main.dart';
import 'package:integration_test/integration_test.dart';

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  testWidgets('Testing out the bloc counter.', (tester) async {
    await tester.pumpWidget(const StateTesting());

    final Finder hamburgerButton = find.byIcon(Icons.menu);

    await tester.tap(hamburgerButton);
    await tester.pumpAndSettle();

    final Finder blocButton = find.text("bloc");

    await tester.tap(blocButton);
    await tester.pumpAndSettle();

    expect(find.text('Current count: [0]'), findsOneWidget);

    final Finder plusButton = find.byIcon(Icons.add);

    await tester.tap(plusButton);
    await tester.pumpAndSettle();

    expect(find.text('Current count: [1]'), findsOneWidget);

    final Finder minusButton = find.byIcon(Icons.remove);

    await tester.tap(minusButton);
    await tester.tap(minusButton);
    await tester.pumpAndSettle();

    expect(find.text('Current count: [0]'), findsOneWidget);

    await tester.tap(hamburgerButton);
    await tester.pumpAndSettle();

    final Finder cameraButton = find.text("camera");

    await tester.tap(cameraButton);
    await tester.pump(const Duration(seconds: 4));

    final Finder switchCameraButton =
        find.widgetWithIcon(FilledButton, Icons.switch_camera);

    await tester.tap(switchCameraButton);
    await tester.pump(const Duration(seconds: 2));

    final Finder takePictureButton =
        find.widgetWithIcon(FilledButton, Icons.camera);

    await tester.tap(takePictureButton);
    await tester.pumpAndSettle();
  });
}
