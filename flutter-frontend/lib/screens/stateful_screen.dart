import 'package:flutter/material.dart';

class StatefulScreen extends StatelessWidget {
  const StatefulScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(child: Text('stateful screen')),
    );
  }
}
