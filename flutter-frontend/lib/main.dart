import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:state_testing/camera/services/notification_service.dart';

import 'bloc_counter/screens/screen.dart';
import 'screens/stateful_screen.dart';
import 'screens/welcome_screen.dart';
import 'camera/screens/camera_scaffold.dart';
import 'package:firebase_core/firebase_core.dart';

import 'firebase_options.dart';

@pragma('vm:entry-point')
Future<void> _backgroundHandler(RemoteMessage message) async {}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  final fcmToken = await FirebaseMessaging.instance.getToken();
  debugPrint(fcmToken);

  NotificationService()
    ..requestNotificationPermission()
    ..firebaseInit();

  if (Platform.isIOS || Platform.isAndroid) {
    FirebaseMessaging.onBackgroundMessage(_backgroundHandler);
  }

  runApp(const StateTesting());
}

class StateTesting extends StatelessWidget {
  const StateTesting({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
        routerConfig: _router,
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.purple),
          useMaterial3: true,
        ));
  }
}

final _shellNavigatorKey = GlobalKey<NavigatorState>();

final GoRouter _router = GoRouter(
  routes: [
    ShellRoute(
        navigatorKey: _shellNavigatorKey,
        pageBuilder: (context, state, child) =>
            NoTransitionPage(child: RootScreen(child: child)),
        routes: [
          GoRoute(
            path: '/',
            builder: (BuildContext context, GoRouterState state) =>
                const WelcomeScreen(),
            routes: [
              GoRoute(
                path: 'stateful',
                builder: (BuildContext context, GoRouterState state) =>
                    const StatefulScreen(),
              ),
              GoRoute(
                path: 'bloc',
                builder: (BuildContext context, GoRouterState state) =>
                    const BlocScreen(),
              ),
              GoRoute(
                path: 'camera',
                builder: (BuildContext context, GoRouterState state) =>
                    const CameraGalleryNavigator(),
              )
            ],
          ),
        ])
  ],
);

class RootScreen extends StatelessWidget {
  final Widget child;
  const RootScreen({super.key, required this.child});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(child: child),
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: const Text('State management'),
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            ListTile(
                title: const Text('welcome'),
                onTap: () {
                  context.go('/');
                  Navigator.pop(context);
                }),
            ListTile(
                title: const Text('stateful'),
                onTap: () {
                  context.go('/stateful');
                  Navigator.pop(context);
                }),
            ListTile(
                title: const Text('bloc'),
                onTap: () {
                  context.go('/bloc');
                  Navigator.pop(context);
                }),
            ListTile(
                title: const Text("camera"),
                onTap: () {
                  context.go("/camera");
                  Navigator.pop(context);
                }),
          ],
        ),
      ),
    );
  }
}
