import 'dart:math';

abstract class RandomState {
  late int _count;
  RandomIncrementState init() => RandomIncrementState().._count = 0;
  int get count => _count;
  final _random = Random();
}

class RandomIncrementState extends RandomState {
  // Setting the upper bound so that it cannot go above 100.
  RandomIncrementState increment(RandomState currentstate) =>
      RandomIncrementState()
        .._count = currentstate.count
        .._count += _random.nextInt(101 - count);
}

class RandomDecrementState extends RandomState {
  // Setting the lower bound so that it cannot go below 0.
  RandomDecrementState decrement(RandomState currentstate) =>
      RandomDecrementState()
        .._count = currentstate.count
        .._count -= _random.nextInt(count + 1);
}
