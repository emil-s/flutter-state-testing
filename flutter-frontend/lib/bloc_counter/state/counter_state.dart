class CounterState {
  late int _count;
  int get count => _count;

  CounterState init() => CounterState().._count = 0;

  CounterState increment(CounterState currentState) => CounterState()
    .._count = currentState.count
    .._count += 1;

  CounterState decrement(CounterState currentState) => CounterState()
    .._count = currentState.count
    .._count = count == 0 ? 0 : _count - 1;
}
