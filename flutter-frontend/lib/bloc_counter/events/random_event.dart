abstract class RandomEvent {}

class RandomIncrement extends RandomEvent {}

class RandomDecrement extends RandomEvent {}
