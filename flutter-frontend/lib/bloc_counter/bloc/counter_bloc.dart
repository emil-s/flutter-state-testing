import 'package:flutter_bloc/flutter_bloc.dart';

import '../events/counter_event.dart';
import '../state/counter_state.dart';

class CounterBloc extends Bloc<CounterEvent, CounterState> {
  CounterBloc() : super(CounterState().init()) {
    on<Increment>((event, emit) => emit(state.increment(state)));
    on<Decrement>((event, emit) => emit(state.decrement(state)));
  }
}
