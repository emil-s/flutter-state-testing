import 'package:flutter_bloc/flutter_bloc.dart';

import '../events/random_event.dart';
import '../state/random_state.dart';

class RandomBloc extends Bloc<RandomEvent, RandomState> {
  RandomBloc() : super(RandomIncrementState().init()) {
    on<RandomIncrement>(
        ((event, emit) => emit(RandomIncrementState().increment(state))));
    on<RandomDecrement>(
        ((event, emit) => emit(RandomDecrementState().decrement(state))));
  }
}
