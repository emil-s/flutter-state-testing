import 'action_buttons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/counter_bloc.dart';
import '../bloc/random_bloc.dart';
import '../state/counter_state.dart';
import '../state/random_state.dart';

class BlocScreen extends StatelessWidget {
  const BlocScreen({super.key});

  @override
  Widget build(BuildContext context) => MultiBlocProvider(providers: [
        BlocProvider<CounterBloc>(create: (context) => CounterBloc()),
        BlocProvider<RandomBloc>(create: (context) => RandomBloc()),
      ], child: const NumberScreen());
}

class NumberScreen extends StatelessWidget {
  const NumberScreen({super.key});

  @override
  Widget build(BuildContext context) => Scaffold(
        body: Center(
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          BlocListener<CounterBloc, CounterState>(
              listener: (context, state) =>
                  _doMessageIfRequired(state.count, context),
              child: BlocBuilder<CounterBloc, CounterState>(
                builder: (context, state) =>
                    Text('Current count: [${state.count}]'),
              )),
          BlocListener<RandomBloc, RandomState>(
              listener: (context, state) =>
                  _doMessageIfRequired(state.count, context),
              child: BlocBuilder<RandomBloc, RandomState>(
                builder: (context, state) =>
                    Text('Current random count: [${state.count}]'),
              )),
          const SizedBox(height: 10),
          const ActionButtons(),
        ])),
      );

  void _doMessageIfRequired(int count, BuildContext context) {
    if (count % 3 == 0) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('THREEEEEEE! $count'),
          duration: const Duration(seconds: 1),
        ),
      );
    }
  }
}
