import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/counter_bloc.dart';
import '../bloc/random_bloc.dart';
import '../events/counter_event.dart';
import '../events/random_event.dart';
import '../state/counter_state.dart';
import '../state/random_state.dart';

class ActionButtons extends StatelessWidget {
  const ActionButtons({super.key});

  @override
  Widget build(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          BlocBuilder<CounterBloc, CounterState>(
              builder: (context, state) => Row(children: [
                    FilledButton(
                      onPressed: () => state.count == 100
                          ? null
                          : context.read<CounterBloc>().add(Increment()),
                      child: const Icon(Icons.add),
                    ),
                    const SizedBox(width: 10),
                    FilledButton(
                      onPressed: () => state.count == 0
                          ? null
                          : context.read<CounterBloc>().add(Decrement()),
                      child: const Icon(Icons.remove),
                    ),
                  ])),
          const SizedBox(width: 10),
          BlocBuilder<RandomBloc, RandomState>(
              builder: (context, state) => Row(children: [
                    FilledButton(
                      onPressed: () => state.count == 100
                          ? null
                          : BlocProvider.of<RandomBloc>(context)
                              .add(RandomIncrement()),
                      child: const Icon(Icons.ramen_dining),
                    ),
                    const SizedBox(width: 10),
                    FilledButton(
                      onPressed: () => state.count == 0
                          ? null
                          : context.read<RandomBloc>().add(RandomDecrement()),
                      child: const Icon(Icons.icecream),
                    ),
                  ]))
        ],
      );
}
