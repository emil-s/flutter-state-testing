abstract class CameraEvent {}

class CameraInitialized extends CameraEvent {}

class CameraSwitched extends CameraEvent {}

class CameraCaptured extends CameraEvent {}
