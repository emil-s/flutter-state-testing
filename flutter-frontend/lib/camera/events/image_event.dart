import '../models/image_model.dart';

abstract class ImageEvent {}

class LoadImages extends ImageEvent {}

class UploadImage extends ImageEvent {
  final ImageModel image;

  UploadImage(this.image);
}
