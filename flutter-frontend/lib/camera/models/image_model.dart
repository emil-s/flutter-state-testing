class ImageModel {
  final String base64Data;
  final String name;
  final String format;

  ImageModel(
      {required this.base64Data, required this.name, required this.format});

  // From JSON
  factory ImageModel.fromJson(Map<String, dynamic> json) {
    return ImageModel(
      base64Data: json['data'],
      name: json['name'],
      format: json['format'],
    );
  }

  // To JSON
  Map<String, String> toJson() {
    return {
      'data': base64Data,
      'name': name,
      'format': format,
    };
  }
}
