import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/image_bloc.dart';
import '../events/image_event.dart';
import '../models/image_model.dart';
import '../state/image_state.dart';

class ImageGalleryPage extends StatefulWidget {
  const ImageGalleryPage({super.key});

  @override
  State<ImageGalleryPage> createState() => _ImageGalleryPageState();
}

class _ImageGalleryPageState extends State<ImageGalleryPage> {
  List<ImageModel> bulletinBoardImages = [];
  Map<String, Offset> imagePositions = {};

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: DragTarget<ImageModel>(
              onAccept: (image) {
                setState(() {
                  bulletinBoardImages.add(image);
                });
              },
              builder: (context, candidateData, rejectedData) =>
                  bulletinBoard(),
            ),
          ),
          SizedBox(
            height: 100,
            child: BlocBuilder<ImageBloc, ImageState>(
              builder: (context, state) {
                if (state is ImagesLoading) {
                  return const Center(child: CircularProgressIndicator());
                } else if (state is ImagesLoadSuccess) {
                  return imageGridView(state);
                }
                return const Center(child: Text('Failed to load images'));
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget bulletinBoard() {
    return Container(
      color: Colors.amber[100],
      child: Stack(
        children: bulletinBoardImages.map((image) {
          Offset position = imagePositions[image.name] ??
              const Offset(20, 20); // Default position
          return Positioned(
            left: position.dx,
            top: position.dy,
            child: Draggable<ImageModel>(
              data: image,
              feedback: Material(
                elevation: 4.0,
                child: Image.memory(base64Decode(image.base64Data),
                    width: 100, height: 100),
              ),
              childWhenDragging: Opacity(
                opacity: 0.5,
                child: Image.memory(base64Decode(image.base64Data)),
              ),
              onDragEnd: (dragDetails) {
                // Setting it to the new position.
                setState(() {
                  final RenderBox renderBox =
                      context.findRenderObject() as RenderBox;
                  final Offset localOffset =
                      renderBox.globalToLocal(dragDetails.offset);
                  imagePositions[image.name] = localOffset;
                });
              },
              child: Image.memory(base64Decode(image.base64Data)),
            ),
          );
        }).toList(),
      ),
    );
  }

  Widget imageGridView(ImagesLoadSuccess state) {
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: state.images.length,
      itemBuilder: (context, index) {
        final image = state.images[index];
        return LongPressDraggable<ImageModel>(
            data: image,
            feedback: Material(
              elevation: 4.0,
              child: Image.memory(base64Decode(image.base64Data),
                  width: 100, height: 100),
            ),
            childWhenDragging: const Icon(Icons.photo),
            child: Image.memory(base64Decode(image.base64Data)));
      },
    );
  }

  @override
  void initState() {
    super.initState();
    context.read<ImageBloc>().add(LoadImages());
  }
}
