import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';
import 'package:state_testing/camera/screens/authentication_screen.dart';
import 'package:state_testing/camera/screens/camera_screen.dart';
import 'package:state_testing/camera/screens/image_screen.dart';

import '../bloc/image_bloc.dart';
import '../provider/auth_provider.dart';
import '../repositories/api_image_repository.dart';

class CameraGalleryNavigator extends StatelessWidget {
  const CameraGalleryNavigator({super.key});

  @override
  Widget build(BuildContext context) {
    final GoRouter router = GoRouter(
      routes: [
        ShellRoute(
          builder: (context, state, child) => CameraScaffold(child: child),
          routes: [
            GoRoute(
              path: '/',
              builder: (BuildContext context, GoRouterState state) =>
                  const AuthenticationScreen(),
            ),
            GoRoute(
              path: '/camera',
              builder: (BuildContext context, GoRouterState state) =>
                  const CameraScreen(),
            ),
            GoRoute(
                path: '/images',
                builder: (BuildContext context, GoRouterState state) =>
                    const ImageGalleryPage()),
          ],
        ),
      ],
    );

    return MultiBlocProvider(
        providers: [
          BlocProvider(
              create: (context) => ImageBloc(repository: ApiImageRepository())),
          ChangeNotifierProvider(create: (context) => AuthProvider()),
        ],
        child: Router(
          routeInformationProvider: router.routeInformationProvider,
          routerDelegate: router.routerDelegate,
          routeInformationParser: router.routeInformationParser,
        ));
  }
}

class CameraScaffold extends StatelessWidget {
  final Widget child;

  const CameraScaffold({super.key, required this.child});

  @override
  Widget build(BuildContext context) => ChangeNotifierProvider(
      create: (context) => NavigationIndexModel(),
      child: Theme(
          data: ThemeData(
            useMaterial3: true,
            colorScheme: ColorScheme.fromSeed(
                seedColor: Theme.of(context).colorScheme.primary,
                brightness: Brightness.dark),
          ),
          child: Scaffold(body:
              Consumer<NavigationIndexModel>(builder: (context, value, _) {
            return Column(children: [
              Expanded(child: child),
              BottomNavigationBar(
                  currentIndex: value.currentIndex,
                  onTap: (index) {
                    context.go(switch (index) {
                      0 => '/',
                      1 => '/camera',
                      _ => '/images',
                    });
                    value.updateIndex(index);
                  },
                  items: const [
                    BottomNavigationBarItem(
                        icon: Icon(Icons.lock), label: 'Authenticate'),
                    BottomNavigationBarItem(
                        icon: Icon(Icons.camera), label: 'Camera'),
                    BottomNavigationBarItem(
                        icon: Icon(Icons.photo), label: 'Images'),
                  ])
            ]);
          }))));
}

class NavigationIndexModel extends ChangeNotifier {
  int _currentIndex = 0;

  int get currentIndex => _currentIndex;

  void updateIndex(int newIndex) {
    _currentIndex = newIndex;
    notifyListeners();
  }
}
