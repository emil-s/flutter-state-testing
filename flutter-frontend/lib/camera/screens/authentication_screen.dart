import 'package:flutter/material.dart';
import 'package:state_testing/camera/screens/widgets/authentication.dart';

class AuthenticationScreen extends StatelessWidget {
  const AuthenticationScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const AuthenticationWidget();
  }
}
