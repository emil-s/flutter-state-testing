import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/image_bloc.dart';
import '../events/image_event.dart';
import 'widgets/camera.dart';

class CameraScreen extends StatelessWidget {
  const CameraScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return CameraWidget(
        onPictureTaken: (imageModel) =>
            context.read<ImageBloc>().add(UploadImage(imageModel)));
  }
}
