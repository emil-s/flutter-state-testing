import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../provider/auth_provider.dart';

class AuthenticationWidget extends StatelessWidget {
  const AuthenticationWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _authenticationView(context),
            const SizedBox(height: 20),
            // Add Consumer here to listen for login status changes
            Consumer<AuthProvider>(
              builder: (context, authProvider, child) {
                return FutureBuilder<String?>(
                  future: authProvider.getToken(),
                  builder: (context, snapshot) => snapshot.data != null
                      ? _logoutButton(context)
                      : Container(),
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _logoutButton(BuildContext context) {
    return ElevatedButton(
      onPressed: () =>
          Provider.of<AuthProvider>(context, listen: false).logout(),
      child: const Text('Logout'),
    );
  }

  Widget _authenticationView(BuildContext context) {
    final TextEditingController usernameController = TextEditingController();
    final TextEditingController passwordController = TextEditingController();

    return Column(
      children: <Widget>[
        TextField(
          controller: usernameController,
          decoration: const InputDecoration(labelText: 'Username'),
        ),
        TextField(
          controller: passwordController,
          obscureText: true,
          decoration: const InputDecoration(labelText: 'Password'),
        ),
        const SizedBox(height: 20),
        ElevatedButton(
          onPressed: () => Provider.of<AuthProvider>(context, listen: false)
              .login(usernameController.text, passwordController.text),
          child: const Text('Login'),
        ),
      ],
    );
  }
}
