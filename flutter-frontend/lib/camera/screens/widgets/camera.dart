import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/camera_bloc.dart';
import '../../events/camera_event.dart';
import '../../models/image_model.dart';
import '../../state/camera_state.dart';

class CameraWidget extends StatelessWidget {
  final void Function(ImageModel) onPictureTaken;

  const CameraWidget({super.key, required this.onPictureTaken});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (context) => CameraBloc()..add(CameraInitialized()),
        child: BlocListener<CameraBloc, CameraState>(
            listener: (context, state) {
              //Do a callback on the resulting image if a picture is taken.
              if (state is PictureTaken) {
                WidgetsBinding.instance.addPostFrameCallback(
                    (_) => onPictureTaken(state.imagePath));
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Text('Took picture: [${state.imagePath.name}]'),
                  duration: const Duration(milliseconds: 1500),
                ));
              } else if (state is CameraFailure) {
                ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(content: Text('Camera Error')));
              }
            },
            child: BlocBuilder<CameraBloc, CameraState>(
              // If the state is CameraReady, show the _cameraView.
              builder: (context, state) => state is CameraReady
                  ? _cameraView(context, state)
                  : const Center(child: CircularProgressIndicator()),
            )));
  }

  Widget _cameraView(BuildContext context, CameraReady state) =>
      Column(children: [
        Expanded(
          child: state.cameraPreview,
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              FilledButton.tonal(
                onPressed: () =>
                    context.read<CameraBloc>().add(CameraSwitched()),
                child: const Icon(Icons.switch_camera),
              ),
              const SizedBox(width: 8),
              FilledButton.tonal(
                onPressed: () =>
                    context.read<CameraBloc>().add(CameraCaptured()),
                child: const Icon(Icons.camera),
              ),
            ],
          ),
        ),
      ]);
}
