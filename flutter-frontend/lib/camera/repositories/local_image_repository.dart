import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:state_testing/camera/models/image_model.dart';
import 'package:state_testing/camera/repositories/image_repository.dart';

class LocalImageRepository implements ImageRepository {
  final _storage = const FlutterSecureStorage();
  final String _imagesKey = 'localImages';

  @override
  Future<List<ImageModel>> getImages() async {
    final imagesString = await _storage.read(key: _imagesKey);
    if (imagesString == null) {
      return [];
    }
    final List<dynamic> imageJsonList = json.decode(imagesString);
    return imageJsonList.map((json) => ImageModel.fromJson(json)).toList();
  }

  @override
  Future<void> putImage(ImageModel image) async {
    List<ImageModel> images = await getImages();
    images.add(image);
    final String updatedImagesString =
        json.encode(images.map((image) => image.toJson()).toList());
    await _storage.write(key: _imagesKey, value: updatedImagesString);
  }
}
