import 'dart:convert';
import 'dart:isolate';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart';
import '../models/image_model.dart';
import 'image_repository.dart';

class ApiImageRepository implements ImageRepository {
  final String _baseUrl = 'http://10.0.2.2:3000';
  final _storage = const FlutterSecureStorage();

  @override
  Future<List<ImageModel>> getImages() async {
    final token = await _storage.read(key: 'accessToken');

    final Response response = await Isolate.run(() async {
      return await http.get(
        Uri.parse('$_baseUrl/images'),
        headers: {'Authorization': 'Bearer $token'},
      );
    });

    if (response.statusCode != 200) {
      throw Exception('Failed to load images');
    }

    final List<dynamic> imageJsonList = json.decode(response.body);
    return imageJsonList.map((json) => ImageModel.fromJson(json)).toList();
  }

  @override
  Future<void> putImage(ImageModel image) async {
    final token = await _storage.read(key: 'accessToken');
    final jsonImage = image.toJson();

    debugPrint(token);

    final Response response = await Isolate.run(() async {
      return await http.put(Uri.parse('$_baseUrl/image'),
          body: jsonEncode(jsonImage),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer $token'
          });
    });

    debugPrint('$response.statusCode');

    if (response.statusCode != 201) {
      throw Exception('Failed to upload image');
    }
  }
}
