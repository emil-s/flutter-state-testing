import '../models/image_model.dart';

abstract class ImageRepository {
  Future<List<ImageModel>> getImages();

  Future<void> putImage(ImageModel image);
}
