import 'dart:convert';

import 'package:camera/camera.dart';
import 'package:flutter/foundation.dart';
import 'package:state_testing/camera/models/image_model.dart';

abstract class CameraState {
  late CameraController _controller;
  late CameraDescription _activeCamera;
  late CameraDescription _inactiveCamera;

  CameraPreview get cameraPreview => CameraPreview(_controller);
}

class CameraInitial extends CameraState {}

class CameraFailure extends CameraState {}

class CameraReady extends CameraState {
  CameraState toReady(CameraState currentState) => this
    .._controller = currentState._controller
    .._activeCamera = currentState._activeCamera
    .._inactiveCamera = currentState._inactiveCamera;

  Future<CameraState> init() async {
    List<CameraDescription> cameras = await availableCameras();
    _activeCamera = cameras[1];
    _inactiveCamera = cameras[0];

    _controller = CameraController(_activeCamera, ResolutionPreset.medium,
        enableAudio: false);
    await _controller.initialize();

    return this;
  }

  Future<CameraState> switchCamera(CameraState currentState) async {
    _activeCamera = currentState._inactiveCamera;
    _inactiveCamera = currentState._activeCamera;

    _controller = CameraController(_activeCamera, ResolutionPreset.medium,
        enableAudio: false);
    await _controller.initialize();

    return this;
  }
}

class PictureTaken extends CameraState {
  late ImageModel imagePath;

  Future<CameraState> takePicture(CameraState currentState) async {
    _controller = currentState._controller;
    _activeCamera = currentState._activeCamera;
    _inactiveCamera = currentState._inactiveCamera;

    XFile file = await _controller.takePicture();
    Uint8List imageBytes = await file.readAsBytes();

    String base64Image = await compute(base64Encode, imageBytes);

    ImageModel imageModel = ImageModel(
        base64Data: base64Image,
        name: file.name,
        format: file.path.split('.').last.toLowerCase());

    imagePath = imageModel;

    return this;
  }
}
