import '../models/image_model.dart';

abstract class ImageState {}

class ImagesInitial extends ImageState {}

class ImagesLoading extends ImageState {}

class ImagesLoadSuccess extends ImageState {
  final List<ImageModel> images;

  ImagesLoadSuccess(this.images);
}

class ImageUploadSuccess extends ImageState {}

class ImageOperationFailure extends ImageState {}
