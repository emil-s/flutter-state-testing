import 'package:flutter_bloc/flutter_bloc.dart';

import '../events/image_event.dart';
import '../repositories/image_repository.dart';
import '../state/image_state.dart';

class ImageBloc extends Bloc<ImageEvent, ImageState> {
  final ImageRepository repository;

  ImageBloc({required this.repository}) : super(ImagesInitial()) {
    on<LoadImages>(_onLoadImages);
    on<UploadImage>(_onUploadImage);
  }

  Future<void> _onLoadImages(LoadImages event, Emitter<ImageState> emit) async {
    emit(ImagesLoading());
    try {
      final images = await repository.getImages();
      emit(ImagesLoadSuccess(images));
    } catch (_) {
      emit(ImageOperationFailure());
    }
  }

  Future<void> _onUploadImage(
      UploadImage event, Emitter<ImageState> emit) async {
    try {
      await repository.putImage(event.image);
      emit(ImageUploadSuccess());
    } catch (_) {
      emit(ImageOperationFailure());
    }
  }
}
