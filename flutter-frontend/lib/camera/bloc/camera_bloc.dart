import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../events/camera_event.dart';
import '../state/camera_state.dart';

class CameraBloc extends Bloc<CameraEvent, CameraState> {
  CameraBloc() : super(CameraInitial()) {
    on<CameraInitialized>((event, emit) async {
      try {
        emit(await CameraReady().init());
      } catch (_) {
        onFailure(emit);
      }
    });

    on<CameraCaptured>((event, emit) async {
      try {
        emit(await PictureTaken().takePicture(state));
        emit(CameraReady().toReady(state));
      } catch (_) {
        onFailure(emit);
      }
    });

    on<CameraSwitched>((event, emit) async {
      try {
        emit(await CameraReady().switchCamera(state));
      } catch (_) {
        onFailure(emit);
      }
    });
  }

  void onFailure(Emitter<CameraState> emit) {
    debugPrint("Camera failure has occured.");
    emit(CameraFailure());
  }
}
