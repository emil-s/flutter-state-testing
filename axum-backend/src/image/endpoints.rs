use axum::{extract::State, http::StatusCode, Json};
use rayon::prelude::*;
use sqlx::SqlitePool;

use crate::image::*;

pub async fn post_image(
    State(pool): State<SqlitePool>,
    Json(payload): Json<Base64Image>,
) -> StatusCode {
    // If the image cannot be decoded from base64, return code 400.
    let Ok(image): Result<ByteImage, _> = payload.try_into() else {
        return StatusCode::BAD_REQUEST;
    };

    // If image cannot be put into the database, return code 500, otherwise return code 201.
    match sqlx::query!(
        "insert into images (name, format, data) VALUES (?1, ?2, ?3)",
        image.name,
        image.format,
        image.data
    )
    .execute(&pool)
    .await
    {
        Ok(_) => StatusCode::CREATED,
        Err(_) => StatusCode::INTERNAL_SERVER_ERROR,
    }
}

pub async fn get_images(
    State(pool): State<SqlitePool>,
) -> Result<Json<Vec<Base64Image>>, StatusCode> {
    // Get all images from database, if not possible, return code 500.
    let Ok(images) = sqlx::query_as!(ByteImage, "SELECT name, format, data FROM images")
        .fetch_all(&pool)
        .await
    else {
        return Err(StatusCode::INTERNAL_SERVER_ERROR);
    };

    // Go through all the images in parallel, convert to base64, encode into json, and return.
    Ok(Json(images.into_par_iter().map(|x| x.into()).collect()))
}
