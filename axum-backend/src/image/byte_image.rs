use super::Base64Image;
use base64::{engine::general_purpose, DecodeError, Engine as _};
use serde::{Deserialize, Serialize};

/// Binary version of an image, will be put into, and returned from the database.
#[derive(Serialize, Deserialize)]
pub struct ByteImage {
    pub name: String,
    pub format: String,
    pub data: Vec<u8>,
}

// Trait that allows us to easily convert to a Base64Image.
impl TryFrom<Base64Image> for ByteImage {
    type Error = DecodeError;

    fn try_from(value: Base64Image) -> Result<Self, Self::Error> {
        Ok(Self {
            name: value.name,
            format: value.format,
            data: general_purpose::STANDARD.decode(value.data)?,
        })
    }
}
