use super::ByteImage;
use base64::{engine::general_purpose, Engine as _};
use serde::{Deserialize, Serialize};

/// Base64 version of an image, will be encoded into json, and sent from/to our client.
#[derive(Serialize, Deserialize)]
pub struct Base64Image {
    pub name: String,
    pub format: String,
    pub data: String,
}

// Trait that allows us to easily convert to BinaryImage.
impl From<ByteImage> for Base64Image {
    fn from(value: ByteImage) -> Self {
        Self {
            name: value.name,
            format: value.format,
            data: general_purpose::STANDARD.encode(value.data),
        }
    }
}
