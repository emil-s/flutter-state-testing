mod authentication;
mod image;

use anyhow::Result;
use axum::{
    middleware::from_fn,
    routing::{get, post, put},
    Router,
};
use dotenv_codegen::*;
use sqlx::{migrate::MigrateDatabase as _, Pool, Sqlite, SqlitePool};

use authentication::*;
use image::*;

#[tokio::main]
async fn main() -> Result<()> {
    let pool = initialize_database(dotenv!("DATABASE_URL")).await?;

    // Set up logging and the way it's formatted.
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::TRACE)
        .without_time()
        .pretty()
        .init();

    // Router with jwt and a handler to the database.
    let image_router = Router::new()
        .route("/images", get(get_images))
        .route("/image", put(post_image))
        .with_state(pool)
        .layer(from_fn(check_token));

    // Router that does not require jwt headers to connect to.
    let login_router = Router::new().route("/login", post(login));

    // Merge the two routers with the tracing layer for deployment.
    let merged = Router::new()
        .merge(image_router)
        .merge(login_router)
        .layer(tower_http::trace::TraceLayer::new_for_http());

    let listener = tokio::net::TcpListener::bind("127.0.0.1:3000").await?;

    println!("Running REST API...");
    axum::serve(listener, merged).await?;

    Ok(())
}

/// If the database already exists, connect to it and return the connection to the caller.
/// If not, then create a new database, connect to it, set up images table, and return the connection.
async fn initialize_database(db_url: &str) -> Result<Pool<Sqlite>> {
    if Sqlite::database_exists(db_url).await.unwrap_or(false) {
        return Ok(SqlitePool::connect(db_url).await?);
    }

    println!("Creating database...");

    Sqlite::create_database(db_url).await?;

    let pool = SqlitePool::connect(db_url).await?;

    sqlx::query!(
        "CREATE TABLE IF NOT EXISTS images (
        name TEXT PRIMARY KEY NOT NULL,
        format TEXT NOT NULL,
        data BLOB NOT NULL) ;",
    )
    .execute(&pool)
    .await?;

    Ok(pool)
}
