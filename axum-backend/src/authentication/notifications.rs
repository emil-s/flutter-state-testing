use anyhow::Result;
use dotenv_codegen::*;
use fcm_v1::{
    android::{AndroidConfig, AndroidMessagePriority},
    auth::Authenticator,
    message::{Message, Notification},
    Client,
};
use std::time::Duration;

const SERVICE_ACCOUNT: &'static str = dotenv!("SERVICE_ACCOUNT_PATH");
const FCM_PROJECT_ID: &'static str = dotenv!("FCM_PROJECT_ID");

pub async fn send_fcm_notification(username: String) -> Result<()> {
    let authenticator = Authenticator::service_account_from_file(SERVICE_ACCOUNT).await?;
    let client = Client::new(
        authenticator,
        FCM_PROJECT_ID,
        false,
        Duration::from_secs(60),
    );

    let notification = Notification {
        title: Some("Wrong password attempted.".into()),
        body: Some(format!(
            "Someone has tried to log into user: [{username}] using the wrong password!"
        )),
        image: None,
    };

    let message = Message {
        android: Some(AndroidConfig {
            priority: Some(AndroidMessagePriority::High),
            ..Default::default()
        }),
        topic: Some("state".into()),
        notification: Some(notification),
        ..Default::default()
    };

    client.send(&message).await?;

    Ok(())
}
