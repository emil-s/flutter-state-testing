use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct Credentials {
    username: String,
    password: String,
}

impl Credentials {
    /// Returns an empty Ok if successful login.
    /// Returns an Err with the username of the account the attempt with on otherwise.
    pub fn check_login(self) -> Result<(), String> {
        match self {
            x if x.username == "my username" && x.password == "my password" => Ok(()),
            x => Err(x.username),
        }
    }
}
