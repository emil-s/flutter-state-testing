use chrono::{Duration, Utc};
use serde::{Deserialize, Serialize};

/// This is the structure of our claims for the JWT token.
#[derive(Debug, Serialize, Deserialize)]
pub struct Claims {
    sub: String,
    aud: String,
    exp: usize,
}

impl Claims {
    pub fn new(sub: impl Into<String>, aud: impl Into<String>) -> Self {
        // Always sets the expiration time to 6 hours from now.
        let exp = Utc::now()
            .checked_add_signed(Duration::try_hours(6).unwrap())
            .unwrap()
            .timestamp() as usize;

        Self {
            sub: sub.into(),
            aud: aud.into(),
            exp,
        }
    }
}
