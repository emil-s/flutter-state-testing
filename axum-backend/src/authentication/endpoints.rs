use axum::{http::StatusCode, Json};

use crate::{authentication::claims::Claims, claims_to_token};

use super::{
    credentials::Credentials, notifications::send_fcm_notification, token_response::TokenResponse,
};

/// We check if the login is correct, then generate and return a JWT token if true.
pub async fn login(Json(payload): Json<Credentials>) -> Result<Json<TokenResponse>, StatusCode> {
    match payload.check_login() {
        Ok(_) => Ok(Json(claims_to_token(Claims::new("images", "user")))),
        Err(username) => {
            let _ = send_fcm_notification(username).await;
            Err(StatusCode::UNAUTHORIZED)
        }
    }
}
