use anyhow::Result;
use fcm_v1::{
    android::{AndroidConfig, AndroidMessagePriority},
    auth::Authenticator,
    message::{Message, Notification},
    Client,
};
use std::time::Duration;

#[tokio::main]
async fn main() -> Result<()> {
    send_fcm_notification().await?;

    Ok(())
}

async fn send_fcm_notification() -> Result<()> {
    let authenticator =
        Authenticator::service_account_from_file("service-account-key.json").await?;

    let client = Client::new(
        authenticator,
        "state-management-cf76b",
        false,
        Duration::from_secs(60),
    );

    let notification = Notification {
        title: Some("Notification Title".into()),
        body: Some("Some important message".into()),
        image: None,
    };

    let message = Message {
        android: Some(AndroidConfig {
            priority: Some(AndroidMessagePriority::High),
            ..Default::default()
        }),
        topic: Some("state".into()),
        notification: Some(notification),
        ..Default::default()
    };

    let response = client.send(&message).await?;
    println!("FCM Response: {:?}", response);

    Ok(())
}
